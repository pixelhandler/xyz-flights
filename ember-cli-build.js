/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var autoprefixer = require('autoprefixer');
var cssnext = require('postcss-cssnext');
var cssimport = require('postcss-import');
var emberTesting = process.env.EMBER_CLI_TEST_COMMAND;

module.exports = function(defaults) {
  var env = EmberApp.env();
  var isProductionBuild = (env === 'production');
  var options = {
    vendorFiles: { "ember.js": false, "jquery.js": false },
    fingerprint: { enabled: isProductionBuild },
    sourcemaps: { enabled: !isProductionBuild },
    minifyCSS: { enabled: isProductionBuild },
    minifyJS: { enabled: isProductionBuild },
    tests: emberTesting || !isProductionBuild,
    hinting: emberTesting || !isProductionBuild,
    postcssOptions: {
      plugins: [
        { module: autoprefixer, options: { browsers: ['last 2 version'] } },
        { module: cssimport },
        { module: cssnext, options: { sourcemap: true } }
      ]
    },
    babel: { includePolyfill: false }
  };

  var app = new EmberApp(defaults, options);

  return app.toTree();
};
